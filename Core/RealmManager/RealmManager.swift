//
//  RealmManager.swift
//  Weather
//
//  Created by Oleksandr Hryhorchuk on 31.05.2018.
//  Copyright � 2018 Oleksandr Hryhorchuk. All rights reserved.
//

import Foundation
import RealmSwift

public final class RealmManager {

	// MARK: - Write Queue

	private static let writeDispatchQueue = DispatchQueue(label: "RealmManager.write.queue")

	// MARK: - Realm

	static var realm: Realm? {
		do {
			return try Realm()
		} catch {
			print("Cannot create Realm with error: \(error)")
			return nil
		}
	}
    
}

// MARK: - Setup

public extension RealmManager {

	static func setup(configuration: Realm.Configuration? = nil) {
		let config = Realm.Configuration(schemaVersion: 0, migrationBlock: { (migration: Migration, oldSchemaVersion: UInt64) in
			print("Executing empty migration Block for old SchemeVersion: \(oldSchemaVersion)")
		})
        
		Realm.Configuration.defaultConfiguration = (configuration ?? config)
        print(Realm.Configuration.defaultConfiguration.fileURL ?? "Realm fileURL is nil")
	}
    
    static func setup(with name: String) {
        
        var configuration = Realm.Configuration()
        configuration.fileURL = configuration.fileURL!.deletingLastPathComponent().appendingPathComponent("\(name).realm")
        
        self.setup(configuration: configuration)
    }
    
}

//MAKR: - Fetch

extension RealmManager {
    
    static func getFirst<T: Object>(objects type: T.Type) -> T? {
        return realm?.objects(type).first
    }
    
    static func getAll<T: Object>(objects type: T.Type) -> [T] {
        return realm?.objects(type).toArray(type: type) ?? []
    }
    
    static func get<T: Object>(objects type: T.Type, filter: String) -> [T] {
        return realm?.objects(type).filter(filter).toArray(type: type) ?? []
    }
    
}

// MARK: - Write

extension RealmManager {

	// MARK: - Dispatch Sync Write Block

	private static func write(block: ((_ realm: Realm) -> Void)) {
		self.writeDispatchQueue.sync {
			guard let realm = self.realm else {
				return
			}
            
			do {
				try realm.write {
					block(realm)
				}
			} catch {
				print("Cannot write in realm with error: \(error)")
			}
		}
	}

	// MARK: - Save
    
	public static func save(_ saveBlock: (() -> Void), completion completionBlock: (() -> Void)?) {
		self.write { (realm: Realm) in
			saveBlock()

			DispatchQueue.main.async {
				completionBlock?()
			}
		}
	}
    
    // MARK: - Save object
    
    public static func save(object: Object, update: Bool = false, completion completionBlock: (() -> Void)?) {
        self.save({
            self.realm?.add(object, update: update)
        }, completion: completionBlock)
    }
    
}

// MARK: - Helper

extension Results {
    
    func toArray<T>(type: T.Type) -> [T] {
        return compactMap { $0 as? T }
    }
    
}
