//
//  CityWeather.swift
//  Weather
//
//  Created by Oleksandr Hryhorchuk on 31.05.2018.
//  Copyright © 2018 Oleksandr Hryhorchuk. All rights reserved.
//

import Alamofire
import ObjectMapper

final class CityWeatherAPI {
    
    enum Route {
        case byCityName(String, Int)
        
        var path: String {
            switch self {
            case .byCityName(let cityName, let countDays):
                return ConstantAPI.byCityName + cityName + ConstantAPI.appID + ConstantAPI.countDays + "\(countDays)"
            }
        }
    }
    
    static func get(
        by name: String,
        and countDays: Int,
        handler: @escaping (_ object: City?, _ error: Error?) -> Void) {
        let request = Route.byCityName(name, countDays)
        
        Alamofire.request(request.path).responseJSON { response in
            switch response.result {
            case .success (let json):
                let city = Mapper<City>().map(JSONObject: json)
                handler(city, nil)
                
            case .failure(let error):
                handler(nil, error)
            }
        }
    }
    
}
