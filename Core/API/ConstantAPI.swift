//
//  API.swift
//  Weather
//
//  Created by Oleksandr Hryhorchuk on 31.05.2018.
//  Copyright © 2018 Oleksandr Hryhorchuk. All rights reserved.
//

import Foundation

final class ConstantAPI {
    
    static var apiKey: String {
        return "049ef87cc995fc320416dbc9ae8ce551"
    }
    
    static var appID: String {
        return "&appid=" + apiKey
    }
    
    static var baseUrl: String {
        return "http://api.openweathermap.org/data/2.5/forecast/daily?"
    }
    
    static var byCityName: String {
        return baseUrl + "q="
    }
    
    static var countDays: String {
        return "&cnt="
    }
    
    static var icon: String {
        return "http://openweathermap.org/img/w/"
    }
    
}
