//
//  Bundle + Extension.swift
//  Weather
//
//  Created by Oleksandr on 5/31/18.
//  Copyright © 2018 com.dev. All rights reserved.
//

import Foundation

extension Bundle {
    
    static var appName: String {
        guard let bundleName = main.object(forInfoDictionaryKey: "CFBundleName") as? String else {
            return "database"
        }
        return bundleName
    }
    
}
