//
//  UIView + Extension.swift
//  Weather
//
//  Created by Oleksandr on 5/31/18.
//  Copyright © 2018 com.dev. All rights reserved.
//

import UIKit
import RSLoadingView

extension UIView {
    
    func startLoading() {
        RSLoadingView(effectType: .twins).show(on: self)
    }
    
    func stopLoading() {
        RSLoadingView.hide(from: self)
    }
    
    func addCornerRadius() {
        layer.cornerRadius = 5.0
    }
    
}
