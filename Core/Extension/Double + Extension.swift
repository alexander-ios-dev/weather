//
//  Double + Extension.swift
//  Weather
//
//  Created by Oleksandr on 5/31/18.
//  Copyright © 2018 com.dev. All rights reserved.
//

import Foundation

extension Double {
    
    func kelvinToCelsius() -> Int {
        let celsius = Int(self - 273.15)
        return celsius
    }
    
}
