//
//  WeatherService.swift
//  Weather
//
//  Created by Oleksandr on 5/31/18.
//  Copyright © 2018 com.dev. All rights reserved.
//

import Foundation

final class WeatherService {
    
    static func getWeathers(
        for term: WeatherTerm,
        from cityName: String,
        handler: @escaping (_ objects: [WeatherDay]?, _ error: Error?) -> Void) {
        if ReachabilityService.isConnected() {
            CityWeatherAPI.get(by: cityName, and: term.rawValue, handler: { city, error in
                CityWeatherService.save(city: city)
                handler(city?.weatherDaysArray, error)
            })
        } else {
            CityWeatherService.get(by: cityName, and: term.rawValue, handler: { weatherDays in
                handler(weatherDays, nil)
            })
        }
    }
    
}
