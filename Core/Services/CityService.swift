//
//  CityService.swift
//  Weather
//
//  Created by Oleksandr on 5/31/18.
//  Copyright © 2018 com.dev. All rights reserved.
//

import Foundation

final class CityWeatherService {
    
    static func get(
        by cityName: String,
        and countDays: Int,
        handler: @escaping (_ objects: [WeatherDay]?) -> Void) {
        let city = RealmManager.get(objects: City.self, filter: "name == '\(cityName)'").first
        
        if let weatherDays = city?.weatherDays.sorted(by: { $0.id > $1.id }) {
            let weatherDaysArray = Array(weatherDays.prefix(countDays))
            handler(weatherDaysArray)
        } else {
            handler(nil)
        }
    }
    
    static func save(city: City?) {
        guard let newCity = city else {
            return
        }
        RealmManager.save(object: newCity, update: true) {
            print("City saved")
        }
    }
    
}
