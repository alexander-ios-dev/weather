//
//  UserDefault.swift
//  Weather
//
//  Created by Oleksandr Hryhorchuk on 31.05.2018.
//  Copyright © 2018 com.dev. All rights reserved.
//

import Foundation

final class UserDefault {
    
    static let shared = UserDefault()
    
    private init() {}
    
    private let defaults = UserDefaults.standard
    
    func term(_ weatherTerm: WeatherTerm) {
        defaults.set(weatherTerm.rawValue, forKey: "weather_term")
        defaults.synchronize()
    }
    
    func term() -> WeatherTerm {
        let term = defaults.integer(forKey: "weather_term")
        return WeatherTerm(rawValue: term) ?? .fiveDays
    }
    
}
