//
//  AppNavigator.swift
//  Weather
//
//  Created by Oleksandr on 5/31/18.
//  Copyright © 2018 com.dev. All rights reserved.
//

import UIKit

final class AppNavigator {
    
    class func setupRootController(for window: UIWindow?) {
        window?.rootViewController = R.storyboard.cityWeather.cityWeatherController()
        window?.makeKeyAndVisible()
    }
    
}
