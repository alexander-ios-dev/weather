//
//  WeatherDay.swift
//  Weather
//
//  Created by Oleksandr on 5/31/18.
//  Copyright © 2018 com.dev. All rights reserved.
//

import RealmSwift
import ObjectMapper

final class WeatherDay: Object, Mappable {
    
    @objc dynamic var id = 0
    @objc dynamic var tempMin = 0.0
    @objc dynamic var tempMax = 0.0
    @objc dynamic var tempDay = 0.0
    @objc dynamic var icon = ""
    @objc dynamic var definition = ""
    
    var dateString: String {
        return id.toDateString() ?? ""
    }
    
    var iconUrl: URL? {
        return URL(string: ConstantAPI.icon + "\(icon)" + ".png")
    }
    
    var dailyTemperature: String {
        return "\(tempDay.kelvinToCelsius())" + " °C"
    }
    
    var minimumTemperature: String {
        return "\(tempMin.kelvinToCelsius())" + " °C"
    }
    
    var maximunTemperature: String {
        return "\(tempMax.kelvinToCelsius())" + " °C"
    }
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    // Mappable
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        self.id         <- map["dt"]
        self.tempMin    <- map["temp.min"]
        self.tempMax    <- map["temp.max"]
        self.tempDay    <- map["temp.day"]
        self.icon       <- map["weather.0.icon"]
        self.definition <- map["weather.0.main"]
    }
    
}
