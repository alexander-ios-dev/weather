//
//  City.swift
//  Weather
//
//  Created by Oleksandr Hryhorchuk on 31.05.2018.
//  Copyright © 2018 Oleksandr Hryhorchuk. All rights reserved.
//

import RealmSwift
import ObjectMapper

final class City: Object, Mappable {
    
    @objc dynamic var id = 0
    @objc dynamic var name = ""
    @objc dynamic var latitude = 0.0
    @objc dynamic var longitude = 0.0
    
    var weatherDays = List<WeatherDay>()
    
    var weatherDaysArray: [WeatherDay] {
        return Array(weatherDays)
    }
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    // Mappable
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        self.id             <- map["city.id"]
        self.name           <- map["city.name"]
        self.latitude       <- map["city.coord.lat"]
        self.longitude      <- map["city.coord.lon"]
        self.weatherDays    <- (map["list"], ArrayTransformService<WeatherDay>())
    }
    
}
