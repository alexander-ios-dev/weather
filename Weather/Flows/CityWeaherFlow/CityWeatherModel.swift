//
//  CityWeatherController.swift
//  Weather
//
//  Created by Oleksandr Hryhorchuk on 31.05.2018.
//  Copyright © 2018 Oleksandr Hryhorchuk. All rights reserved.
//

import UIKit
import Foundation

protocol CityWeatherView: class {
    
    func startLoading()
    func stopLoading()
    func unknownError(_ error: Error?)
    func update()
}

enum WeatherTerm: Int {
    
    case fiveDays = 5, tenDays = 10, fifteenDays = 15
    
    var label: String {
        
        //* Move to Localizable file */
        switch self {
        case .fiveDays: return "5 days"
        case .tenDays: return "10 days"
        case .fifteenDays: return "15 days"
        }
    }
}

final class CityWeatherModel {
    
    weak var view: CityWeatherView?
    
    var weatherDays: [WeatherDay] = []
    
    init(view: CityWeatherView) {
        self.view = view
    }
    
    func get(for term: WeatherTerm, from cityName: String) {
        view?.startLoading()
        
        WeatherService.getWeathers(for: term, from: cityName) { [weak self] weatherDays, error in
            self?.view?.stopLoading()
            
            guard let weathers = weatherDays else {
                self?.view?.unknownError(error)
                return
            }
            self?.weatherDays = weathers
            self?.view?.update()
        }
    }
    
    func getWeather() -> (temperatures: [CGFloat], days: [String], count: CGFloat) {
        let temperatures: [CGFloat] = weatherDays.map { CGFloat($0.tempDay.kelvinToCelsius()) }
        let days = weatherDays.map { $0.dateString }
        let count = CGFloat(weatherDays.count)
        
        return (temperatures: temperatures, days: days, count: count)
    }
}
