//
//  CityWeatherViewController.swift
//  Weather
//
//  Created by Oleksandr Hryhorchuk on 31.05.2018.
//  Copyright © 2018 Oleksandr Hryhorchuk. All rights reserved.
//

import UIKit

private let cityName = "Kiev"

final class CityWeatherController: UIViewController {
    
    @IBOutlet private var cityNameLabel: UILabel!
    @IBOutlet private var countDaysLabel: UILabel!
    @IBOutlet private var collectionView: UICollectionView!
    @IBOutlet private var viewForLineChart: UIView!
    @IBOutlet private var selectTermButton: UIButton!
    
    private var lineChart: LineChart!
    private var model: CityWeatherModel!
    private var cityWeatherDataSource: CityWeatherDataSource!
    
    private var term: WeatherTerm {
        set {
            UserDefault.shared.term(newValue)
        }
        get {
            return UserDefault.shared.term()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        registerNib()
        setupController()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        prepareData()
    }
    
    @IBAction func selectTerm(_ sender: Any) {
        DialogHelper.showTermSelect(on: self) { [weak self] weatherTerm in
            self?.term = weatherTerm
            self?.prepareData()
        }
    }
    
    private func setupUI() {
        isHiddentViews(true)
        cityNameLabel.text = cityName
        countDaysLabel.text = term.label
    }
    
    private func setupController() {
        model = CityWeatherModel(view: self)
        cityWeatherDataSource = CityWeatherDataSource(model: model)
        collectionView.dataSource = cityWeatherDataSource
    }
    
    private func prepareData() {
        model?.get(for: term, from: cityName)
    }
    
    private func registerNib() {
        collectionView.register(R.nib.dayCollectionViewCell)
    }
    
    private func move(at index: Int) {
        let indexPath = IndexPath(row: index, section: 0)
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
    
    private func updateChart() {
        if lineChart != nil {
            lineChart.removeFromSuperview()
        }
        
        lineChart = LineChartHelper.setup(from: model, and: viewForLineChart.bounds)
        lineChart.delegate = self
        viewForLineChart.addSubview(lineChart)
    }
    
    private func isHiddentViews(_ isHidden: Bool) {
        countDaysLabel.isHidden = isHidden
        collectionView.isHidden = isHidden
        viewForLineChart.isHidden = isHidden
        selectTermButton.isHidden = isHidden
    }

}

extension CityWeatherController: CityWeatherView {
    
    func startLoading() {
        view.startLoading()
    }
    
    func stopLoading() {
        view.stopLoading()
    }
    
    func unknownError(_ error: Error?) {
        DialogHelper.showError(on: self, with: error?.localizedDescription)
    }
    
    func update() {
        isHiddentViews(false)
        updateChart()
        collectionView.reloadData()
    }
    
}

extension CityWeatherController: LineChartDelegate {
    
    func didSelectDataPoint(_ x: CGFloat, yValues: Array<CGFloat>) {
        move(at: Int(x))
    }
    
}
