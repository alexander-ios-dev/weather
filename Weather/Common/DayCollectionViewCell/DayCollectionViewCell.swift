//
//  DayCollectionViewCell.swift
//  Weather
//
//  Created by Oleksandr on 5/31/18.
//  Copyright © 2018 com.dev. All rights reserved.
//

import UIKit
import SDWebImage

final class DayCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet private var customView: UIView!
    @IBOutlet private var dateLabel: UILabel!
    @IBOutlet private var dayTempLabel: UILabel!
    @IBOutlet private var minTempLabel: UILabel!
    @IBOutlet private var maxTempLabel: UILabel!
    @IBOutlet private var iconImageView: UIImageView!
    
    func fill(from weatherDay: WeatherDay) {
        customView.addCornerRadius()
        
        dateLabel.text = weatherDay.dateString
        dayTempLabel.text = weatherDay.dailyTemperature
        //* Move to Localizable file */
        minTempLabel.text = "min. " + weatherDay.minimumTemperature
        maxTempLabel.text = "max. " + weatherDay.maximunTemperature
        
        iconImageView.sd_setImage(with: weatherDay.iconUrl, completed: nil)
    }
    
}
