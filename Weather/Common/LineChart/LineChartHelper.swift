//
//  LineChartHelper.swift
//  Weather
//
//  Created by Oleksandr on 5/31/18.
//  Copyright © 2018 com.dev. All rights reserved.
//

import UIKit

final class LineChartHelper {
    
    static func setup(from model: CityWeatherModel, and frame: CGRect) -> LineChart {
        let weather = model.getWeather()
        let lineChart = LineChart()
        lineChart.animation.enabled = true
        lineChart.area = true
        lineChart.x.grid.count = weather.count
        lineChart.y.grid.count = weather.count
        lineChart.x.labels.values = weather.days
        lineChart.addLine(weather.temperatures)
        lineChart.frame = frame
        
        return lineChart
    }
    
}
