//
//  DialogHelper.swift
//  Weather
//
//  Created by Oleksandr on 5/31/18.
//  Copyright © 2018 com.dev. All rights reserved.
//

import UIKit
import AZDialogView

final class DialogHelper {
    
    static func showError(on controller: UIViewController, with description: String?) {
        //* Move to Localizable file */
        let dialog = AZDialogViewController(title: "Error", message: description)
        dialog.addAction(AZDialogAction(title: "Ok") { dialog in
            dialog.dismiss()
        })
        dialog.show(in: controller)
    }
    
    static func showTermSelect(
        on controller: UIViewController,
        handler: @escaping (_ object: WeatherTerm) -> Void) {
        //* Move to Localizable file */
        let dialog = AZDialogViewController(title: "Select term")
        dialog.addAction(getAction(with: .fiveDays, handler: handler))
        dialog.addAction(getAction(with: .tenDays, handler: handler))
        dialog.addAction(getAction(with: .fifteenDays, handler: handler))
        dialog.show(in: controller)
    }
    
    static private func getAction(
        with term: WeatherTerm,
        handler: @escaping (_ object: WeatherTerm) -> Void) -> AZDialogAction {
        return AZDialogAction(title: term.label) { dialog in
            handler(term)
            dialog.dismiss()
        }
    }
    
}
