//
//  DataSource.swift
//  Weather
//
//  Created by Oleksandr Hryhorchuk on 31.05.2018.
//  Copyright © 2018 com.dev. All rights reserved.
//

import UIKit

final class CityWeatherDataSource: NSObject, UICollectionViewDataSource {
    
    private var model: CityWeatherModel!
    
    init(model: CityWeatherModel) {
        self.model = model
    }
    
    func collectionView(
        _ collectionView: UICollectionView,
        numberOfItemsInSection section: Int) -> Int {
        return model.weatherDays.count
    }
    
    func collectionView(
        _ collectionView: UICollectionView,
        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let weatherDay = model.weatherDays[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.dayWeatherCell.identifier, for: indexPath) as! DayCollectionViewCell
        cell.fill(from: weatherDay)
        
        return cell
    }
    
}
