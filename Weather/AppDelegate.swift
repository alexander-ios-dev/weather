//
//  AppDelegate.swift
//  Weather
//
//  Created by Oleksandr on 5/31/18.
//  Copyright © 2018 com.dev. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        RealmManager.setup(with: Bundle.appName)
        AppNavigator.setupRootController(for: window)
        
        return true
    }
}
